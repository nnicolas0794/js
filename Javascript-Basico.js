// Ejercicio 1:

//abcde
/*abcde*/ 

// Ejercicio 2: 
var myName ;


// Ejercicio 3:
var a;
a = 7;


// Ejercicio 4:
var a;
a = 7;
var b;
b = 7;
b = a;

// Ejercicio 5:
var myVar = 0;
var a = 9;


// Ejercicos 6: 
var myFirstName ='larry'
var myLastName = 'capinga'

// Ejercico 7:
var a = 5;
var b = 10;
var c ="I am a";


a = a + 1;
b = b + 5;
c = c + " String!";

// Ejercicio 8:
var studlyCapVar;
var properCamelCase;
var titleCaseOver;

// Variable assignments
studlyCapVar = 10;
properCamelCase = "A String";
titleCaseOver = 9000;

// Ejercicio 9: 
var catName = "Oliver";
var catSound = "Meow!";

// Ejercicio 10:
const FCC = "freeCodeCamp"; 
let fact = "is cool!"; 
fact = "is awesome!";
console.log(FCC, fact); 

// Ejercicio 11: 
const sum = 10 + 10;

// Ejercicio: 12:
const difference = 45 - 33;

// Ejercicio 13:
const product = 8 * 10;

// Ejercicio 14:
const quotient1 = 66 / 33;

// Ejercicio 15:
let myVar = 87;
myVar ++;

// Ejercicio 16:
let myVar = 11;
myVar --;

// Ejercicio 17:
const ourDecimal = 5.7;
let myDecimal = 5.7

// Ejercicio 18:
const product = 2.0 * 2.5;

// Ejercicio 19: 
const quotient = 4.4 / 2.0; 

// Ejercicio 20: 
const remainder = 11 % 3;

// Ejercicio 21:
let a = 3;
let b = 17;
let c = 12;

a += a + 9;
b += 26 - b;
c += c - 5;

// Ejercicio 22: 
let a = 11;
let b = 9;
let c = 3;

a -= a - 5;
b -= b + 6;
c -= c - 2;

// Ejercicio 23:
let a = 5;
let b = 12;
let c = 4.6;

a *=  5;
b *= 3 ;
c *=  10;

//Ejercicio 24:
let a = 48;
let b = 108;
let c = 33;

a /=  12;
b /=  4;
c /=  11;

// Ejercicio 25:
const myStr = "I am a \"double quoted\" string inside \"double quotes\"."; 

// Ejercicio 26:
const myStr = '<a href="http://www.example.com" target="_blank">Link</a>';

// Ejercicio 27:
const myStr = "FirstLine\n\t\\SecondLine\nThirdLine";

// Ejercicio: 28: 
const myStr = "This is the start."+" This is the end.";

// Ejercicio: 29: 
let myStr ="This is the first sentence. ";
myStr+="This is the second sentence.";

//Ejercicio 30:
const myName = "Nicolas";
const myStr = "My name is "+myName+"and I am well!";


//Ejercicio 31:
const someAdjective = "dificult";
let myStr = "Learning to code is ";
myStr += someAdjective;

//Ejercicio 32:
let lastNameLength = 0;
const lastName = "Lovelace";

lastNameLength = lastName.length;

//Ejercicio 33:
let firstLetterOfLastName = "";
const lastName = "Lovelace";

firstLetterOfLastName = lastName[0];

//Ejercicio 34:
let myStr = "Jello World";

myStr = "Hello World";

//Ejercicio 35:
const lastName = "Lovelace";

const thirdLetterOfLastName = lastName[2]; 

//Ejercicio 36:
const lastName = "Lovelace";

const lastLetterOfLastName = lastName[lastName.length -1];

//Ejercicio 37: 
// Setup
const lastName = "Lovelace";

const secondToLastLetterOfLastName = lastName[lastName.length - 2];

//Ejercicio 38:
const myNoun = "dog";
const myAdjective = "big";
const myVerb = "ran";
const myAdverb = "quickly";

const wordBlanks = myNoun +" "+myAdjective+" "+myVerb+" "+myAdverb;

//Ejercicio 39:
const myArray = ["1",2];

//Ejercicio 40:
const myArray = [["Bulls", 23],22];

//Ejercicio 41:
const myArray = [50, 60, 70];
let myData = myArray[0]

//Ejercicio 42:
const myArray = [18, 64, 99];

myArray[0] = 45;