// Ejercicio 1 :Basic Data Structures(arrays)
let yourArray = [1,2,3,true,'5'];

// Ejercicio 2 :
let myArray = ["a", "b", "c", "d"];

myArray[1] = 'x';

console.log(myArray);
// Ejercicio 3 :
function mixedNumbers(arr) {
    arr.unshift("I", 2, "three");
    arr.push(7, "VIII", 9);
    return arr;
}
  
console.log(mixedNumbers(['IV', 5, 'six']));

// Ejercicio 4 : 
function popShift(arr) {
  let popped = arr.pop()
  let shifted = arr.shift()
  return [shifted, popped];
}

console.log(popShift(['challenge', 'is', 'not', 'complete']));

// Ejercicio 5 :
const arr = [2, 4, 5, 1, 7, 5, 2, 1];
arr.splice(1,4)
console.log(arr);

// Ejercicio 6 : 
function htmlColorNames(arr) {
arr.splice(0,2,"DarkSalmon","BlanchedAlmond")
  return arr;
}

console.log(htmlColorNames(['DarkGoldenRod', 'WhiteSmoke', 'LavenderBlush','PaleTurquoise', 'FireBrick']));

// Ejercicio 7;
function forecast(arr) {
  return arr.slice(2,4);
}

console.log(forecast(['cold', 'rainy', 'rwam', 'sunny', 'cool', 'thunderstorms']));


// Ejercicio 8;
function copyMachine(arr, num) {
  let newArr = [];
  while (num >= 1) {
newArr.push([...arr])
    num--;
  }
  return newArr;
}
console.log(copyMachine([true, false, true], 2));

// Ejercicio 9: 
function spreadOut() {
  let fragment = ['to', 'code'];
  let sentence=['learning',...fragment,'is', 'fun'];
  return sentence;
}

console.log(spreadOut());
// Ejercicio 10:
function quickCheck(arr, elem) {
if (arr.indexOf(elem) == -1){
return false;
};
return true;
}
console.log(quickCheck(['squash', 'onions', 'shallots'], 'mushrooms'));


// Ejercicio 11:
function filteredArray(arr, elem) {
  let newArr = [];
for (let i=0;i<arr.length;i++){
        if(arr[i].indexOf(elem)>=0){
          delete(arr[i])

        }else{
            newArr.push(arr[i]);
        }
}
  return newArr;
}

console.log(filteredArray([[3, 2, 3], [1, 6, 3], [3, 13, 26], [19, 3, 9]], 3));


// Ejercicio 12:
let myNestedArray = [
  ["unshift", false, 1, 2, 3, "complex", "nested"],
  ["loop", "shift", 6, 7, 1000, "method"],
  ["concat", false, true, "spread", "array", ["deep"]],
  ["mutate", 1327.98, "splice", "slice", "push", [["deeper"]]],
  ["iterate", 1.3849, 7, "8.4876", "arbitrary", "depth", [[["deepest"]]]]
];


// Ejercicio 13:
let foods = {
  apples: 25,
  oranges: 32,
  plums: 28
};

foods["bananas"] = 13
foods["grapes"] = 35
foods["strawberries"] = 27


console.log(foods);

// Ejercicio 14:
let userActivity = {
  id: 23894201352,
  date: 'January 1, 2017',
  data: {
    totalUsers: 51,
    online: 42
  }
};
userActivity.data.online=45

console.log(userActivity);

// Ejercicio 15 :
let foods = {
  apples: 25,
  oranges: 32,
  plums: 28,
  bananas: 13,
  grapes: 35,
  strawberries: 27
};

function checkInventory(scannedItem) {
return foods[scannedItem]
}

console.log(checkInventory("apples"));

// Ejercicio 16:
let foods = {
  apples: 25,
  oranges: 32,
  plums: 28,
  bananas: 13,
  grapes: 35,
  strawberries: 27
};

delete(foods.oranges)
delete(foods.plums)
delete(foods.strawberries)

console.log(foods);

// Ejercicio 17:
let users = {
  Alan: {
    age: 27,
    online: true
  },
  Jeff: {
    age: 32,
    online: true
  },
  Sarah: {
    age: 48,
    online: true
  },
  Ryan: {
    age: 19,
    online: true
  }
};

function isEveryoneHere(userObj) {
   if(userObj.hasOwnProperty('Alan') &&
  userObj.hasOwnProperty('Jeff') &&
  userObj.hasOwnProperty('Sarah') &&
  userObj.hasOwnProperty('Ryan')){
    return(true)
  }
  else{return false}
}

console.log(isEveryoneHere(users));

// Ejercicio 18:
const users = {
  Alan: {
    online: false
  },
  Jeff: {
    online: true
  },
  Sarah: {
    online: false
  }
}

function countOnline(usersObj) {
  let c=0 
for(let user in usersObj){
  if (usersObj[user].online == true)
     c++;
}
  return c;
}

console.log(countOnline(users));

// Ejercicio 19:
let users = {
  Alan: {
    age: 27,
    online: false
  },
  Jeff: {
    age: 32,
    online: true
  },
  Sarah: {
    age: 48,
    online: false
  },
  Ryan: {
    age: 19,
    online: true
  }
};

function getArrayOfUsers(obj) {
return Object.keys(obj)
}

console.log(getArrayOfUsers(users));

// Ejercicio 20:
let user = {
  name: 'Kenneth',
  age: 28,
  data: {
    username: 'kennethCodesAllDay',
    joinDate: 'March 26, 2016',
    organization: 'freeCodeCamp',
    friends: [
      'Sam',
      'Kira',
      'Tomo'
    ],
    location: {
      city: 'San Francisco',
      state: 'CA',
      country: 'USA'
    }
  }
};

function addFriend(userObj, friend) {
userObj.data.friends.push(friend);
return userObj.data.friends;
}

console.log(addFriend(user, 'Pete'));









// Ejercicio 1 : ES6 
function checkScope() {
  let i = 'function scope';
  if (i == true) {
    i = 'block scope';
    console.log('Block scope i is: ', i);
  }
  console.log('Function scope i is: ', i);
  return i;
}

// Ejercicio 2 :
const s = [5, 7, 2];
function editInPlace() {

s.pop()
s.unshift(2)

}
editInPlace();

// Ejercicio 3 :
function freezeObj() {
  const MATH_CONSTANTS = {
    PI: 3.14
  };
Object.freeze(MATH_CONSTANTS)
  try {
    MATH_CONSTANTS.PI = 99;
  } catch(ex) {
    console.log(ex);
  }
  return MATH_CONSTANTS.PI;
}
const PI = freezeObj();


// Ejercicio 4 :
const magic = () => {
  return new Date();
};

// EJercicio 5 :
const myConcat = (arr1, arr2) => {
  return arr1.concat(arr2);
};
console.log(myConcat([1, 2], [3, 4, 5]));

// Ejercicio 6 :

const increment = (number, value = 1) => number + value;


// Ejercicio 7 :
const sum = (...args) => {
  return args.reduce((a, b) => a + b, 0);
}


// Ejercicio 8 :
const arr1 = ['JAN', 'FEB', 'MAR', 'APR', 'MAY'];
let arr2;

arr2 = [...arr1];  

console.log(arr2);

// Ejercicio 9 :

const HIGH_TEMPERATURES = {
  yesterday: 75,
  today: 77,
  tomorrow: 80
};

const {today, tomorrow} = HIGH_TEMPERATURES;



// Ejercicio 10 :
const HIGH_TEMPERATURES = {
  yesterday: 75,
  today: 77,
  tomorrow: 80
};
 
const {today:highToday,tomorrow:highTomorrow} = HIGH_TEMPERATURES 

// Ejercicio 11 :
const LOCAL_FORECAST = {
  yesterday: { low: 61, high: 75 },
  today: { low: 64, high: 77 },
  tomorrow: { low: 68, high: 80 }
};

  
  const{today :{low:lowToday,high:highToday }} =LOCAL_FORECAST;

// Ejercicio 12 :

let a = 8, b = 6;
 [a,b] = [b,a]

// Ejercicio 13 :

function removeFirstTwo(list) {
  const [x,y,...shorterList] = list;
  return shorterList;
}

const source = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10];
const sourceWithoutFirstTwo = removeFirstTwo(source);




// Ejercicio 14 :

const stats = {
  max: 56.78,
  standard_deviation: 4.34,
  median: 34.54,
  mode: 23.87,
  min: -0.75,
  average: 35.85
};
const half = ({max,min}) => (max+min)/2;


// Ejercicio 15 :
const result = {
  success: ["max-length", "no-amd", "prefer-arrow-functions"],
  failure: ["no-var", "var-on-top", "linebreak"],
  skipped: ["no-extra-semi", "no-dup-keys"]
};
function makeList(arr) {
  const failureItems = [];
  for (let i=0; i<arr.length ;i++){
 failureItems.push (
   `<li class="text-warning">${result.failure[i]}</li>`
   );
  }
  return failureItems;
}

const failuresList = makeList(result.failure);

//Ejercicio 16 : 
const createPerson = (name, age, gender) => 
    ({name,age,gender});


//Ejercicio 17:
const bicycle = {
  gear: 2,
  setGear(newGear) {
    this.gear = newGear;
  }
};
bicycle.setGear(3);
console.log(bicycle.gear);


// Ejercicio 18:
class Vegetable{
  constructor(name){
    this.name=name
  }
}
const carrot = new Vegetable('carrot');
console.log(carrot.name); 

//Ejercicio 19:
class Thermostat{
  constructor(Fahrenheit){
    this.Fahrenheit=Fahrenheit
  }
  get temperature(){
    return (5/9 * (this.Fahrenheit - 32))
  }
  set temperature(Celsius){
    this.Fahrenheit = (Celsius * 9.0) / 5 + 32
  }
}
const thermos = new Thermostat(76); 
let temp = thermos.temperature; 
thermos.temperature = 26;
temp = thermos.temperature; 


//Ejercicio 20:
<html>
  <body>
<script type="module" src="index.js"></script>
  </body>
</html>


//Ejercicio 21:
const uppercaseString = (string) => {
  return string.toUpperCase();
}
export { uppercaseString };
const lowercaseString = (string) => {
  return string.toLowerCase()
}
export { lowercaseString };

//Ejercicio 22:
import { uppercaseString, lowercaseString  } from './string_functions.js';

uppercaseString("hello");
lowercaseString("WORLD!");

//Ejercicio 23:
import * as stringFunctions from "./string_functions.js";

stringFunctions.uppercaseString("hello");
stringFunctions.lowercaseString("WORLD!");

//Ejercicio 24:
export default function subtract(x, y) {
  return x - y;
}

//Ejercicio 25:
import subtract from "./math_functions.js";

subtract(7,4);

//Ejercicio 26:
const makeServerRequest = new Promise((resolve,reject) => {
});

//Ejercicio 27:
const makeServerRequest = new Promise((resolve, reject) => {
  let responseFromServer;
    
  if(responseFromServer) {
    resolve("We got the data");
  } else {  
    reject("Data not received");
  }
});

//Ejercicio 28:
const makeServerRequest = new Promise((resolve, reject) => {
  makeServerRequest.then(result => {
  let responseFromServer = true;
    
  if(responseFromServer) {
    resolve("We got the data");
  } else {  
    reject("Data not received");
  }
});
console.log(result)
});

//Ejercicio 29
const makeServerRequest = new Promise((resolve, reject) => {
  let responseFromServer = false;
    
  if(responseFromServer) {
    resolve("We got the data");
  } else {  
    reject("Data not received");
  }
});
makeServerRequest.then(result => {
  console.log(result);
});
makeServerRequest.catch(error => {
  console.log(error)
});



















// Ejercicio 1 : Functional Programming

const prepareTea = () => 'greenTea';
const getTea = (numOfCups) => {
    const teaCups = [];
  
    for(let cups = 1; cups <= numOfCups; cups += 1) {
      const teaCup = prepareTea();
      teaCups.push(teaCup);
    }
    return teaCups;
  };
  
 
  const tea4TeamFCC = getTea(40);

// Ejercicio 2 :

const prepareGreenTea = () => 'greenTea';
const prepareBlackTea = () => 'blackTea';

const getTea = (prepareTea, numOfCups) => {
  const teaCups = [];

  for(let cups = 1; cups <= numOfCups; cups += 1) {
    const teaCup = prepareTea();
    teaCups.push(teaCup);
  }
  return teaCups;
};


const tea4GreenTeamFCC = getTea(prepareGreenTea,27);
const tea4BlackTeamFCC = getTea(prepareBlackTea,13);


console.log(
  tea4GreenTeamFCC,
  tea4BlackTeamFCC
);

// Ejercicio 3 :

const Window = function(tabs) {
    this.tabs = tabs; 
  };
  
  Window.prototype.join = function(otherWindow) {
    this.tabs = this.tabs.concat(otherWindow.tabs);
    return this;
  };
  
  Window.prototype.tabOpen = function(tab) {
    this.tabs.push('new tab');
    return this;
  };
  
  Window.prototype.tabClose = function(index) {
  
    const tabsBeforeIndex = this.tabs.splice(0, index); 
    const tabsAfterIndex = this.tabs.splice(1); 
  
    this.tabs = tabsBeforeIndex.concat(tabsAfterIndex); 
  
    return this;
   };
  

  const workWindow = new Window(['GMail', 'Inbox', 'Work mail', 'Docs', 'freeCodeCamp']); 
  const socialWindow = new Window(['FB', 'Gitter', 'Reddit', 'Twitter', 'Medium']); 
  const videoWindow = new Window(['Netflix', 'YouTube', 'Vimeo', 'Vine']); 

  const finalTabs = socialWindow
    .tabOpen() 
    .join(videoWindow.tabClose(2)) 
    .join(workWindow.tabClose(1).tabOpen());
  console.log(finalTabs.tabs);

